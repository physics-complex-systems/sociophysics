"""Record computer experiments.

Make an :class:`Experiment` easier to reproduce by recording its setup and
results.

Example:
    .. code-block:: python

        # Define a full factorial model.
        def my_model(*args, **kwds):
            ...
            return Result(inputs, outputs)

        # Set up an experiment.
        my_experiment = Experiment(my_model)

        # Run it.
        my_experiment.run(*args, **kwds)

        # Save its setup and results to a file.
        my_experiment.to_file(my_file.csv)


.. rubric:: Entity–Relationship Diagram

.. image:: ../_static/er-diagram.svg
    :width: 500 px

..
    erDiagram
        Scientist ||--o{ Experiment: runs
        Experiment ||--|| model: "is defined by"
        model ||..o{ Result: produces
        Experiment ||--o{ Run: records
        Run ||--|| Result: has
        Experiment ||..o{ File: "is saved to"
        Scientist }o..o{ File: analyzes


.. rubric:: Class Diagram

.. image:: ../_static/class-diagram.svg
    :width: 120px
..
    classDiagram
        Experiment "1" *-- "0..*" Run
        Run "1" *-- "1" Result
        class Experiment{
            +model
            +run()
            +to_file()
        }
        class Run{
            +setup
            +start_time
            +runtime
        }
        class Result{
            +inputs
            +outputs
        }
"""

import dataclasses
import importlib.metadata
import inspect
import os
import pathlib
import textwrap
import time
import warnings
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Union

import numpy as np
import pandas as pd


# -- Aliases ------------------------------------------------------------------


ArrayLike = Any  # To be released in numpy.typing 1.20.
DtypeLike = Any  # To be released in numpy.typing 1.20.


# -- Functions ----------------------------------------------------------------


def _uniquify_path(
        path: Union[str, os.PathLike]
) -> pathlib.Path:
    """Make a `path` unique.

    Sequentially insert an increasing number before the suffix of `path` until
    the resulting path is unique.

    Returns:
        A unique path of the form ``parent/stem(integer).suffix``.
    """
    unique_path = path = pathlib.Path(path)
    i = 0
    while unique_path.is_file():
        unique_path = path.with_name(f"{path.stem}({i}){path.suffix}")
        i += 1
    return unique_path


# -- Classes ------------------------------------------------------------------


@dataclasses.dataclass
class Result:
    """The return of an :attr:`Experiment.model`.

    Example:
        .. code-block:: python

            inputs = {
                'x': [x_0, ..., x_M],
                'y': [y_0, ..., y_N],
            }
            outputs = {
                'z': float,
            }
            result = Result(inputs, outputs)
    """

    inputs: Dict[str, np.ndarray]
    """Names and levels of the manipulated (independent) variables.

    Example:
        .. code-block:: python

            inputs = {
                'x': [x_0, ..., x_M],
                'y': [y_0, ..., y_N],
            }
    """
    outputs: Dict[str, np.ndarray]
    r"""Names and values of the measured (dependent) variables.

    Example:
        .. code-block:: python

            outputs = {
                'z': [
                    [u_00, ..., u_0N],
                           ...,
                    [u_M0, ..., u_MN],
                ],
            }

    The values of each measured variable are stored in an
    :math:`n`\ -dimensional array, where :math:`n` is the number of manipulated
    variables.

    More precisely, the array of each measured variable corresponds to the
    cartesian product of the arrays of the manipulated variables—which implies
    they must have the same shape:

    .. code-block:: python

        >>> inputs_shape = tuple(map(len, inputs.values()))
        >>> output_shape = outputs.values()[0].shape
        >>> inputs_shape == output_shape
        True

    Said differently, a measured value with index
    :math:`(i_1, \dots, i_n, \dots)` is observed when the
    :math:`n^{\mathrm{th}}` manipulated variable takes its
    :math:`i_n^{\mathrm{th}}` level.
    """

    def __init__(
            self,
            inputs: Dict[str, ArrayLike],
            outputs: Dict[str, Union[DtypeLike, ArrayLike]]
    ) -> None:
        """Initialize self.

        Parameters:
            inputs:
                Names and levels of the manipulated variables. It must be an
                ordered mapping between variable names and their levels.
            outputs:
                Names and values of the measured variables. It can be a
                mapping between each variable name and:

                * its values, if they are known; or
                * its data type, if the values are to be determined later. In
                  this case, the variable is associated with an array of zeros
                  of the specified data type and appropriate shape.

        Warns:
            UserWarning:
                If the shape of the values of a measured variable is
                inconsistent with the number of levels of the manipulated
                variables.
        """
        self.inputs = {}
        for name, levels in inputs.items():
            self.inputs[name] = np.array(levels, ndmin=1)
        inputs_shape = tuple(map(len, inputs.values()))
        self.outputs = {}
        for name, dtype_or_values in outputs.items():
            try:
                dtype = np.dtype(dtype_or_values)
                values = np.zeros(inputs_shape, dtype=dtype)
            except TypeError:
                values = np.array(dtype_or_values, ndmin=1)
                output_shape = values.shape
                if output_shape == inputs_shape:
                    pass
                elif (
                    len(output_shape) < len(inputs_shape) and
                    [n for n in output_shape if n>1] ==
                    [n for n in inputs_shape if n>1]
                ):
                    values = values.reshape(inputs_shape)
                else:
                    warnings.warn(
                        " ".join([
                            f"The shape {output_shape} of the values of the",
                            f"measured variable '{name}' is inconsistent with",
                            f"the number of levels {inputs_shape} of the",
                            "manipulated variables.",
                        ]),
                        UserWarning
                    )
            self.outputs[name] = values

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            return NotImplemented
        if list(self.inputs) != list(other.inputs):
            return False
        if self.outputs.keys() != other.outputs.keys():
            return False
        for attr in ['inputs', 'outputs']:
            for name, values in getattr(self, attr).items():
                if np.issubdtype(values.dtype, np.inexact):
                    compare = np.allclose
                else:
                    compare = np.array_equal
                if not compare(values, getattr(other, attr)[name]):
                    return False
        return True

    def _to_csv(
            self,
            path: Union[str, os.PathLike],
            *,
            file_header: str = None,
            column_headers: bool = True,
            float_format: str = '%.12g',
    ) -> None:
        """Save the experiment data to a |.csv| file.

        If the file already exists, the data will be appended.

        Parameters:
            path:
                The path to save the file to. Its suffix will be set to |.csv|.
            file_header:
                Text included along the experiment data.
            column_headers:
                Whether to write out the column names.
            float_format:
                Format string for floating point numbers.

        See Also:
            :meth:`pandas.DataFrame.to_csv`

        .. |.csv| replace:: :meth:`.csv <pandas.DataFrame.to_csv>`
        """
        pd_to_csv_kwds = {
            'float_format': float_format,
            'header': column_headers,
            'mode': 'a',
        }
        path = pathlib.Path(path).with_suffix('.csv')
        if file_header is not None:
            with open(path, mode='a', newline="") as file:
                file.write(file_header)
        index = pd.MultiIndex.from_product(
            iterables=self.inputs.values(), names=self.inputs.keys()
        )
        data = {
            name: values.reshape(-1) for name, values in self.outputs.items()
        }
        pd.DataFrame(index=index, data=data).to_csv(path, **pd_to_csv_kwds)

    def _to_npz(
            self,
            path: Union[str, os.PathLike],
            *,
            file_header: str = None
    ) -> None:
        """Save the experiment run to a |.npz| file.

        If the file already exists, it will be overwritten.

        Parameters:
            path:
                The path to save the file to. Its suffix will be set to |.npz|.
            file_header:
                Text included along the experiment data.

        Raises:
            ValueError:
                If the path suffix is not a supported file format.

        See Also:
            :func:`numpy.savez`

        .. |.npz| replace:: :func:`.npz <numpy.savez>`
        """
        path = pathlib.Path(path).with_suffix('.npz')
        if file_header is not None:
            np.savez(path, **self.inputs, **self.outputs, header=file_header)
        else:
            np.savez(path, **self.inputs, **self.outputs)


@dataclasses.dataclass
class Run:
    """The return of an :meth:`Experiment.run`.

    Example:
        .. code-block:: python

            my_run = my_experiment.run(*args, **kwds)
    """

    setup: inspect.BoundArguments
    """The arguments used to call the :attr:`Experiment.model`."""
    start_time: time.struct_time
    """The time at which the :class:`Experiment.run` started, in UTC."""
    runtime: float
    """The duration of the :class:`Experiment.run`, in seconds."""
    result: Result
    """The return of the :attr:`Experiment.model`."""

    def __str__(
            self
    ) -> str:
        time_format: str = '%Y-%m-%dT%H:%MZ'
        return "\n\n".join([
            " ".join([
                "The model ran",
                f"at {time.strftime(time_format, self.start_time)}",
                f"for {self.runtime:.0g} second{'s'*(self.runtime!=1)}.",
            ]),
            Experiment._format_title("Setup"),
            Experiment._dict_to_str(
                self.setup.arguments,
                precision=3, threshold=20, edgeitems=10, max_line_width=80
            ),
        ])


@dataclasses.dataclass
class Experiment:
    """A computer experiment.

    It is defined by a full factorial :attr:`model` that can be :meth:`run` and
    whose :attr:`runs` can be saved :meth:`to_file`.

    Example:
        .. code-block:: python

            # Set up an experiment.
            my_experiment = Experiment(my_model)

            # Run it.
            my_experiment.run(*args, **kwds)

            # Save its setup and results to a file.
            my_experiment.to_file(my_file.csv)
    """

    model: Callable[..., Result]
    """The model defining the experiment.

    * It takes as :py:attr:`~Result.inputs` the levels of the manipulated
      variables.
    * It produces as :py:attr:`~Result.outputs` the values of the measured
      variables.
    * It is expected to be full factorial, that is, produce a value for each
      possible combination of the levels.

    Example:
        .. code-block:: python

            def my_model(*args, **kwds):
                inputs = {
                    'x': [x_0, ..., x_M],
                    'y': [y_0, ..., y_N],
                }
                outputs = {
                    'z': float,
                }
                result = Result(inputs, outputs)

                for i, x_i in enumerate(result.inputs['x']):
                    for j, y_j in enumerate(result.inputs['y']):
                        result.outputs['z'][i, j] = my_calculations(x_i, y_j)

                return result
    """
    runs: List[Run] = dataclasses.field(default_factory=list)
    """Log of :meth:`run` calls."""

    def __str__(
            self
    ) -> str:
        package = self.model.__module__.partition(".")[0]
        try:
            version = f" v{importlib.metadata.version(package)}"
        except importlib.metadata.PackageNotFoundError:
            version = ""
        runs = len(self.runs)
        runtime = sum(run.runtime for run in self.runs)
        return "\n\n".join([
            f"{self.model.__module__}.{self.model.__qualname__}{version}",
            " ".join([
                f"The model ran {runs} time{'s'*(runs!=1)}",
                f"for a total of {runtime:.0g} second{'s'*(runtime!=1)}.",
            ])
        ])

    def run(
            self,
            *args: Any,
            **kwds: Any
    ) -> Run:
        """Run the experiment.

        Call the :attr:`model` and record the resulting :class:`Run` in the
        :attr:`runs` log.

        Example:
            .. code-block:: python

                >>> my_latest_run = my_experiment.run(*args, **kwds)
                >>> my_latest_run == my_experiment.runs[-1]
                True

        Parameters:
            args:
                Positional arguments with which to call the :attr:`model`.
            kwds:
                Keyword arguments with which to call the :attr:`model`.

        Warns:
            UserWarning:
                If the return of the model cannot be coerced to
                :class:`Result`.

        Returns:
            A :class:`Run`, detailing setup and results.
        """
        setup = inspect.signature(self.model).bind(*args, **kwds)
        setup.apply_defaults()
        start = time.time()
        result = self.model(*args, **kwds)
        runtime = time.time() - start
        start_time = time.gmtime(time.time())
        if not isinstance(result, Result):
            try:
                result = Result(**result)
            except TypeError:
                try:
                    result = Result(*result)
                except TypeError:
                    warnings.warn(
                        " ".join([
                            "The return of the model could not be coerced to",
                            "Result.",
                        ]),
                        UserWarning
                    )
        run = Run(setup, start_time, runtime, result)
        self.runs.append(run)
        return run

    def to_file(
            self,
            path: Union[str, os.PathLike],
            *,
            overwrite: bool = False,
            float_format: str = '%.12g'
    ) -> None:
        """Save the experiment to a file.

        * Each :class:`Run` in :attr:`runs` is saved.
        * The supported file formats are |.csv| and |.npz|.

        Example:
            .. code-block:: python

                my_experiment.to_file(my_file.csv)

        Parameters:
            path:
                The path to save the file to. Its suffix is identified as the
                file format.
            float_format:
                Any format string for floating point numbers supported by
                :meth:`pandas.DataFrame.to_csv`. Only relevant if the suffix
                is |.csv|.

        Raises:
            ValueError:
                If the suffix is not a supported file format.

        See Also:
            :meth:`pandas.DataFrame.to_csv`
            :func:`numpy.savez`

        .. |.csv| replace:: :meth:`.csv <pandas.DataFrame.to_csv>`
        .. |.npz| replace:: :func:`.npz <numpy.savez>`
        """
        path = pathlib.Path(path)
        suffix = path.suffix

        # Comma separated values (CSV)

        if suffix == '.csv':
            path = _uniquify_path(path)
            with open(path, 'x') as file:
                file.write(textwrap.indent(
                    f"{str(self)}\n\n",
                    prefix="# ",
                    predicate=lambda line: True
                ))
            for n, run in enumerate(self.runs):
                file_header = textwrap.indent(
                    "\n\n".join([
                        Experiment._format_title(f"Run {n+1}"),
                        str(run),
                        "",
                    ]),
                    prefix="# ",
                    predicate=lambda line: True
                )
                run.result._to_csv(
                    path,
                    file_header=file_header,
                    column_headers=(n==0),
                    float_format=float_format
                )
            return None

        # NumPy zipped archive (NPZ)

        if suffix == '.npz':
            for n, run in enumerate(self.runs):
                path = _uniquify_path(path)
                file_header = "\n\n".join([
                    str(self),
                    Experiment._format_title(f"Run {n+1}"),
                    str(run),
                ])
                run.result._to_npz(path, file_header=file_header)
            return None

        # File format not supported

        raise ValueError(f"File format '{suffix}' not supported.")

    @staticmethod
    def _dict_to_str(
            dictionary: dict,
            **numpy_printoptions: Any
    ) -> str:
        """Cast a `dictionary` as a string.

        Parameters:
            **numpy_printoptions:
                Any keyword argument supported by :func:`numpy.array2string`.
        """
        prefix = 4 * " "
        lines = []
        for key, val in dictionary.items():
            val = np.array2string(np.array(val), **numpy_printoptions)
            lines.extend([f"{key}:", textwrap.indent(f"{val}", prefix=prefix)])
        return "\n".join(lines)

    @staticmethod
    def _format_title(
            title: str
    ) -> str:
        """Format a section title."""
        return f"-- {title + ' ':-<76}"
