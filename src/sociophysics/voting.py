"""Voting module of the sociophysics package.

Functions related to Galam's model [galam]_ for democratic hierarchies under bottom-up
majority rule voting.
"""


from typing import List

import numpy as np
import scipy.stats
import sociophysics.experiment


def grouprate(
        size: int,
        rate: float
) -> float:
    """The expected success rate of a group.

    Calculate the expected success rate of a group when aggregating agents
    whose success rates are equal into groups of fixed size.

    Args:
        size:
            Size of the groups.
        rate:
            Success rate of the agents.

    Returns:
        Expected success rate of the groups.

    See Also:
        :py:func:`~diff_grouprate`

    """
    return scipy.stats.binom.sf(np.ceil(size/2)-1, size, rate)


def diff_grouprate(
        size: int,
        rate: float
) -> float:
    """The derivative of :py:func:`~grouprate` with respect to the rate.

    Calculate the derivative of the expected success rate of the groups
    with respect to the success rate of the agents.

    Args:
        size:
            Size of the groups.
        rate:
            Success rate of the agents.

    Returns:
        Derivative of the expected success rate of the groups with
        respect to the success rate of the agents.

    See Also:
        :py:func:`~grouprate`
    """
    return size * scipy.stats.binom.pmf(np.ceil(size/2)-1, size-1, rate)


def sample_grouprate(
        sizes: List[int],
        iterations: int,
        rates: List[float]
) -> sociophysics.experiment.Result:
    """Sample the group rate function and its iterations."""
    inputs = {
        'size': sizes,
        'iteration': 1 + np.arange(iterations),
        'rate': rates,
    }
    outputs = {
        'grouprate': 'f8',
        'diff_grouprate': 'f8',
    }
    result = sociophysics.experiment.Result(inputs, outputs)

    for i, size in enumerate(sizes):
        diff0 = rates
        diff1 = np.ones_like(rates)

        for j in range(iterations):
            diff1 = diff_grouprate(size, diff0) * diff1
            diff0 = grouprate(size, diff0)

            result.outputs['grouprate'][i, j] = diff0
            result.outputs['diff_grouprate'][i, j] = diff1

    return result


def sample_model(
        sizes: List[int],
        maxlevels: List[int],
        rates: List[float]
) -> sociophysics.experiment.Result:
    """Sample Galam's bottom-up hierarchical voting model."""
    inputs = {
        'size': sizes,
        'maxlevel': maxlevels,
        'rate': rates,
    }
    outputs = {
        'mean_exact': 'f8',
        'mean_approx': 'f8',
    }
    result = sociophysics.experiment.Result(inputs, outputs)

    for i, size in enumerate(sizes):
        for j, maxlevel in enumerate(maxlevels):
            levels = np.arange(0, maxlevel+1)
            agents = size**levels

            for k, rate in enumerate(rates):
                mean_approx = rate
                outcomes = np.arange(0, agents[-1]+1)
                probabilities = scipy.stats.binom.pmf(
                    outcomes,
                    agents[-1],
                    rate
                )
                grouprates = grouprate(size, outcomes/agents[-1])

                mean_approx = grouprate(size, mean_approx)
                mean_exact = grouprates.dot(probabilities)
                outcomes = outcomes[:agents[-2]+1]
                probabilities = scipy.stats.binom.pmf(
                    outcomes,
                    agents[-2],
                    grouprate(size, rate)
                )
                grouprates = grouprates[::size]

                if maxlevel > 1:
                    for level in np.flip(levels[:-2]):  # Fix this.
                        mean_approx = grouprate(size, mean_approx)
                        mean_exact = grouprates.dot(probabilities)
                        outcomes = outcomes[:agents[level]+1]
                        conditionals = scipy.stats.binom.pmf(  # Stratify this.
                            outcomes[:, np.newaxis],
                            agents[level],
                            grouprates
                        )
                        probabilities = conditionals.dot(probabilities)
                        grouprates = grouprates[::size]

                result.outputs['mean_exact'][i, j, k] = mean_exact
                result.outputs['mean_approx'][i, j, k] = mean_approx

    return result
