"""This is the sociophysics package.

Please refer to the documentation provided in the README.rst,
which can be found at https://gitlab.com/physics-complex-systems/sociophysics
"""

import importlib.metadata

__version__ = importlib.metadata.metadata(__package__)['Version']
__author__ = importlib.metadata.metadata(__package__)['Author']
__license__ = importlib.metadata.metadata(__package__)['License']
