.. include:: ../README.rst
   :end-before: inclusion-marker-do-not-remove

.. toctree::
   :caption: Readme
   :maxdepth: 1

   readme
   changelog

.. toctree::
   :caption: Usage
   :maxdepth: 2

   examples
   API Reference <_autosummary/sociophysics>
