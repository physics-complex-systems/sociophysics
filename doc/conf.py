"""Sphinx documentation builder configuration file."""


import importlib.metadata
import time

import sphinx_rtd_theme  # noqa: F401


# -- Parameters ---------------------------------------------------------------


package = "sociophysics"
gitlab_host = "gitlab.com"
gitlab_pages = "gitlab.io"
gitlab_user = "physics-complex-systems"
gitlab_repo = package
gitlab_version = "trunk"


# -- Project information ------------------------------------------------------


project = importlib.metadata.metadata(package)['Name'].capitalize()
version = importlib.metadata.metadata(package)['Version']
release = version
author = importlib.metadata.metadata(package)['Author']
copyright = f"{time.strftime('%Y')}, {author}"


# -- General configuration ----------------------------------------------------


# Glob-style patterns that should be excluded when looking for source files.
exclude_patterns = [
    "_build",
    "**.ipynb_checkpoints",
]

# A list of strings that are module names of extensions.
extensions = [
    'nbsphinx',  # Source parser for *.ipynb files.
    'sphinx.ext.autodoc',  # Include documentation from docstrings.
    'sphinx.ext.autosummary',  # Generate autodoc summaries.
    'sphinx.ext.extlinks',  # Markup to shorten external links.
    'sphinx.ext.intersphinx',  # Link to other projects’ documentation.
    'sphinx.ext.mathjax',  # Render math via JavaScript.
    'sphinx.ext.napoleon',  # Support for NumPy and Google style docstrings.
    'sphinx.ext.todo',  # Support for todo items.
    'sphinx.ext.viewcode',  # Add links to highlighted source code.
    'sphinx_rtd_theme',  # Read the Docs Sphinx Theme.
]

# A list of prefixes that are ignored for sorting the Python module index.
modindex_common_prefix = [
    f"{importlib.metadata.metadata(package)['Name']}.",
]

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# Add any paths that contain templates here, relative to this directory.
templates_path = [
    '_templates',
]


# -- Options for extensions ---------------------------------------------------


# Whether to execute notebooks before conversion or not.
nbsphinx_execute = 'never'

# What content will be inserted into the main body of an autoclass directive.
autoclass_content = 'class'

# The default options for autodoc directives.
autodoc_default_options = {}

# This value selects how automatically documented members are sorted.
autodoc_member_order = 'bysource'

# This value controls how to represent typehints.
autodoc_typehints = 'description'

# List of modules to be mocked up.
autodoc_mock_imports = [
    requirement.split("; ")[0]
    for requirement in importlib.metadata.requires(package)
    if len(requirement.split("; ")) == 1
    or requirement.split("; ")[-1] != 'extra == "doc"'
]

# Generate stub pages for each found document with autosummary directives.
autosummary_generate = True

# Map of unique alias names to the base URL of external sites and a prefix.
extlinks = {}

# Locations and names of other projects that should be linked to.
intersphinx_mapping = {
    'matplotlib': ("https://matplotlib.org", None),
    'numpy': ("https://numpy.org/doc/stable", None),
    'pandas': ("https://pandas.pydata.org/docs", None),
    'python': ("https://docs.python.org/3", None),
    'scipy': ("https://docs.scipy.org/doc/scipy/reference", None),
    'sphinx': ("https://www.sphinx-doc.org/en/stable", None),
}

# False to disable support for NumPy style docstrings.
napoleon_numpy_docstring = False

# True to use the :ivar: role for instance variables.
napoleon_use_ivar = False

# False to output the return type inline with the description.
napoleon_use_rtype = True

# If True, 'todo' and 'todoList' produce output, else they produce nothing.
todo_include_todos = True


# -- Options for HTML output --------------------------------------------------


# Enable 'Edit on GitLab' button.
html_context = {
    'conf_py_path': "/doc/",
    'display_gitlab': True,
    'gitlab_host': gitlab_host,
    'gitlab_user': gitlab_user,
    'gitlab_repo': gitlab_repo,
    'gitlab_version': gitlab_version,
}

# Name of an image file that is the logo of the docs.
html_logo = "_static/logo_100x100_circle.png"

# The theme to use for HTML and HTML Help pages.
html_theme = 'sphinx_rtd_theme'
