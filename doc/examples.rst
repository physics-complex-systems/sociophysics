.. _examples:

Examples
========

Voting
------

.. toctree::
   :glob:
   :maxdepth: 1

   use/voting-sample_grouprate

Other
-----

.. toctree::
   :glob:
   :maxdepth: 1

   use/voting-sample_model
