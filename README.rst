Sociophysics
############


An exploration of sociophysical models
++++++++++++++++++++++++++++++++++++++


|license| |release| |pipeline| |coverage| |status|

.. |license| image:: https://img.shields.io/badge/dynamic/json.svg?label=license&url=https://gitlab.com/api/v4/projects/19084927?license=True&query=$.license.key&colorB=blue
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/tree/trunk/LICENSE

.. |release| image:: https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://gitlab.com/api/v4/projects/19084927/releases&query=$[0].tag_name&colorB=blue
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/releases

.. |pipeline| image:: https://gitlab.com/physics-complex-systems/sociophysics/badges/trunk/pipeline.svg
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/pipelines

.. |coverage| image:: https://gitlab.com/physics-complex-systems/sociophysics/badges/trunk/coverage.svg
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/pipelines

.. |status| image:: https://www.repostatus.org/badges/latest/wip.svg
   :alt: Project status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.
   :target: https://www.repostatus.org/#wip


A collection of scripts to analyze a selection of sociophysical models.

.. inclusion-marker-do-not-remove

Description
===========

Context [galam]_, background, purpose, use, features. Add visuals.

----

Setup
=====

Requirements
------------

Prerequisites, dependencies, etc.

   See the `requirements file`_ for details.

.. _`requirements file`: https://github.com/MiguelGDonoso/sociophysics/blob/master/requirements.txt

Install
-------

How to get the project running, e.g. use the package manager `pip`_ to install Sociophysics.

note:
if myenv is named then --prefix and ./myenv may be replaced by --name and myenv respectievely.

setup

#. Install miniconda
#. conda config --add channels conda-forge
#. conda config --set channel_priority strict
#. conda update --all
#. conda config --set env_prompt "({name}) "

maintenance

#. conda update --all
#. conda clean --all

conda envs

#. conda info --envs
#. conda create --prefix ./myenv
#. conda update --prefix ./myenv --all
#. conda install --prefix ./myenv package_spec
#. conda remove --prefix ./myenv --all
#. conda activate ./myenv
#. conda deactivate

working with environment.yaml

#. conda env create --prefix ./myenv --file environment.yml
#. conda env update --prefix ./myenv --file environment.yml --prune
#. conda env export --no-builds | grep -v -e "^name: " -e "^prefix: " > environment.yml

working with conda local packages

#. conda install conda-build conda-verify
#. conda build conda.recipe
#. conda build purge
#. conda install --prefix ./myenv --channel local mypackage
#. (myenv) python -m pip install --editable .

to use multiple envs in Jupyter (installed in jupyter-env)

#. conda activate ./myenv
#. (myenv) conda install ipykernel
#. (myenv) python -m ipykernel install --name jupyter-env --display-name "myenvname"


.. _pip: https://pip.pypa.io/en/stable/

.. code-block:: bash

   pip install sociophysics

----

Usage
=====

Include :ref:`examples <examples>` , e.g. `examples`_

.. _examples: https://physics-complex-systems.gitlab.io/sociophysics/examples.html

.. code-block:: python

   import sociophysics

   sociophysics.doThis() # does This
   sociophysics.returnThat() # returns That

----

Contact
=======

To request support, report an issue, or contribute, please `📩 contact`__ the author.

.. __: mailto:miguelgdonoso@gmail.com

----

License
=======

This project is licensed under the `📜 BSD-2-Clause License`__. See the `LICENSE file`__ for details.

.. __: https://opensource.org/licenses/BSD-2-Clause
.. __: https://github.com/MiguelGDonoso/sociophysics/blob/master/LICENSE

----

Sources 
=======

Acknowledgements
----------------

* `Semantic Versioning`_
* `Keep a Changelog`_

.. _`Semantic Versioning`: https://semver.org/spec/v2.0.0.html
.. _`Keep a Changelog`: https://keepachangelog.com/en/1.0.0/

Contributors 
------------

* Author - Miguel Gómez Donoso - `MiguelGDonoso`_ - miguelgdonoso@gmail.com

.. _`MiguelGDonoso`: https://github.com/MiguelGDonoso

References
----------

.. [galam] Galam, Sociophysics

