"""Test the experiment module of the sociophysics package."""

import pytest
import sociophysics.experiment


# -- Attributes ---------------------------------------------------------------


inputs = {
    'x1': [0, 1],
    'x2': [2],
    'x3': [3, 4],
}

outputs = {
    'y': [
        [[5, 6]],
        [[6, 7]],
    ]
}

consistent_outputs = {
    'y': [
        [5, 6],
        [6, 7],
    ]
}

inconsistent_outputs = {
    'y': [
        [[[5, 6]]],
        [[[6, 7]]],
    ]
}

dtype_outputs = {
    'y': int,
}

zero_outputs = {
    'y': [
        [[0, 0]],
        [[0, 0]],
    ]
}


# -- Functions ----------------------------------------------------------------


def model(x1, x2, x3, mode=None):
    """Test model."""
    inputs = {
        'x1': x1,
        'x2': x2,
        'x3': x3,
    }
    outputs = {
        'y': int,
    }
    result = sociophysics.experiment.Result(inputs, outputs)

    for i, x1_i in enumerate(x1):
        for j, x2_j in enumerate(x2):
            for k, x3_k in enumerate(x3):
                result.outputs['y'][i, j, k] = x1_i + x2_j + x3_k

    if mode == 'dict_coercible':
        return {'inputs': result.inputs, 'outputs': result.outputs}
    if mode == 'list_coercible':
        return result.inputs, result.outputs
    if mode == 'not_coercible':
        return 'not_coercible'
    return result


# -- Classes ------------------------------------------------------------------


class TestResult:
    """Test the Result class."""

    def test_hasattributes(self):
        """Class instances have the correct attributes."""
        result = sociophysics.experiment.Result(inputs, outputs)
        attrs = [
            'inputs',
            'outputs',
        ]
        assert all(map(hasattr, len(attrs)*[result], attrs))

    def test_consistent_init(self):
        """Consistent outputs can be coerced.

        Outputs whose shape is consistent with the inputs can be coerced to
        match their shapes.
        """
        sociophysics.experiment.Result(inputs, consistent_outputs)

    def test_inconsistent_init(self):
        """Inconsistent outputs cannot be coerced.

        Outputs whose shape is inconsistent with the inputs cannot be coerced
        to match their shapes.
        """
        with pytest.warns(UserWarning):
            sociophysics.experiment.Result(inputs, inconsistent_outputs)

    def test_dtype_init(self):
        """An outputs may be initialized from its data type alone.

        If only the data type of an output is specified, an array of zeros
        consistent with the shapes of the inputs is created.
        """
        actual = sociophysics.experiment.Result(inputs, dtype_outputs)
        expected = sociophysics.experiment.Result(inputs, zero_outputs)
        assert actual == expected


class TestRun:
    """Test the Run class."""

    def test_hasattributes(self):
        """Class instances have the correct attributes."""
        experiment = sociophysics.experiment.Experiment(model)
        run = experiment.run(**inputs)
        attrs = [
            'setup',
            'start_time',
            'runtime',
            'result',
        ]
        assert all(map(hasattr, len(attrs)*[run], attrs))


class TestExperiment:
    """Test the Experiment class."""

    def test_hasattributes(self):
        """Class instances have the correct attributes."""
        experiment = sociophysics.experiment.Experiment(model)
        attrs = [
            'model',
            'runs',
            'run',
            'to_file',
        ]
        assert all(map(hasattr, len(attrs)*[experiment], attrs))

    def test_run_result(self):
        """The run method produces the correct Result."""
        experiment = sociophysics.experiment.Experiment(model)
        run = experiment.run(**inputs)
        actual = run.result
        expected = sociophysics.experiment.Result(inputs, outputs)
        assert actual == expected

    def test_run_dict_coercible(self):
        """Coerce the return of a model to Result if it is a dictionary.

        The keys of the dictionary must be 'inputs' and 'outputs'.
        """
        experiment = sociophysics.experiment.Experiment(model)
        experiment.run(**inputs, mode='dict_coercible')

    def test_run_list_coercible(self):
        """Coerce the return of a model to Result if it is a list.

        The first and second elements of the list must be the inputs and the
        outputs, respectively.
        """
        experiment = sociophysics.experiment.Experiment(model)
        experiment.run(**inputs, mode='list_coercible')

    def test_run_not_coercible(self):
        """Warn if the return of a model cannot be coerced to Result."""
        experiment = sociophysics.experiment.Experiment(model)
        with pytest.warns(UserWarning):
            experiment.run(**inputs, mode='not_coercible')

    def test_runs(self):
        """Runs are logged."""
        experiment = sociophysics.experiment.Experiment(model)
        run = experiment.run(**inputs)
        assert experiment.runs[-1] == run

    @pytest.mark.parametrize(
        "existing",
        [
            pytest.param(0, id="new"),
            pytest.param(1, id="one_exists"),
            pytest.param(2, id="two_exist"),
        ]
    )
    def test_to_file_csv(self, tmp_path, existing):
        """CSV files are produced without overwriting existing files."""
        def name(n):
            return "file.csv" if n == -1 else f"file({n}).csv"
        for i in range(-1, existing-1):
            path = tmp_path / name(i)
            path.touch()
        expected = tmp_path / name(existing-1)
        experiment = sociophysics.experiment.Experiment(model)
        experiment.to_file(tmp_path / "file.csv")
        assert expected.exists()

    @pytest.mark.parametrize(
        "existing",
        [
            pytest.param(0, id="none_exist"),
            pytest.param(1, id="one_exists"),
            pytest.param(2, id="many_exist"),
        ]
    )
    @pytest.mark.parametrize(
        "runs",
        [
            pytest.param(0, id="no_runs"),
            pytest.param(1, id="one_run"),
            pytest.param(2, id="many_runs"),
        ]
    )
    def test_to_file_npz(self, tmp_path, existing, runs):
        """NPZ files are produced without overwriting existing files."""
        def name(n):
            return "file.npz" if n == -1 else f"file({n}).npz"
        for i in range(-1, existing-1):
            path = tmp_path / name(i)
            path.touch()
        experiment = sociophysics.experiment.Experiment(model)
        expected = []
        for i in range(runs-1):
            experiment.run(**inputs)
            expected.append(tmp_path / name(existing+i-1))
        experiment.to_file(tmp_path / "file.npz")
        print(expected)
        assert all(path.exists() for path in expected)

    def test_to_file_suffix_not_supported(self, tmp_path):
        """Raise an error if the suffix is not a supported file format."""
        experiment = sociophysics.experiment.Experiment(model)
        with pytest.raises(ValueError):
            experiment.to_file(tmp_path / "file.ext")
