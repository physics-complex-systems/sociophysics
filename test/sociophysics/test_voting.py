"""Test the voting module of the sociophysics package."""


import pathlib

import numpy as np
import pytest
import sociophysics.voting


# -- Attributes ---------------------------------------------------------------


DATA_DIR = pathlib.Path(__file__).with_suffix("").resolve()


# -- Classes ------------------------------------------------------------------


class TestGrouprate:
    """Test the grouprate function."""

    def test_size_nonnegative_noninteger(self):
        """Non-negative non-integer values raise a warning."""
        with pytest.warns(DeprecationWarning):
            sociophysics.voting.grouprate(np.pi, 0.5)

    @pytest.mark.parametrize(
        "sizes, rates, expected",
        [
            pytest.param(
                [-np.pi, -3, 0, np.e, 4], [-1, -1e-6, 1+1e-6, 2], [np.nan],
                id="invalid_rate",
            ),
            pytest.param(
                [-np.e, -1], np.linspace(start=0, stop=1, num=10), [np.nan],
                id="negative_size",
            ),
            pytest.param(
                [0], np.linspace(start=0, stop=1, num=10), [1],
                id="zero_size",
            ),
            pytest.param(
                np.arange(start=1, stop=11), [0, 1], [0, 1],
                id="positiveinteger_size-endpoint_rate",
            ),
            pytest.param(
                np.arange(start=1, stop=20, step=2), [0.5], [0.5],
                id="odd_size-midpoint_rate",
            ),
            pytest.param(
                [4], [(5 - np.sqrt(13)) / 6], [(5 - np.sqrt(13)) / 6],
                id="size_four-fixed_point",
            ),
        ]
    )
    def test_result(self, sizes, rates, expected):
        """Result is correct."""
        sizes = np.array(sizes)[:, np.newaxis]
        actual = sociophysics.voting.grouprate(sizes, rates)
        expected = np.broadcast_to(expected, actual.shape)
        np.testing.assert_allclose(actual, expected)


class TestSampleGrouprate:
    """Test the sample_grouprate function."""

    def test_sample(self):
        """Result matches the sample."""
        data = np.load(DATA_DIR / "grouprate_sample.npz")
        inputs = {name: data[name] for name in (
            'size',
            'iteration',
            'rate',
        )}
        outputs = {name: data[name] for name in (
            'grouprate',
            'diff_grouprate',
        )}
        expected = sociophysics.experiment.Result(inputs, outputs)
        actual = sociophysics.voting.sample_grouprate(
            sizes=expected.inputs['size'],
            iterations=expected.inputs['iteration'][-1],
            rates=expected.inputs['rate']
        )
        assert actual == expected


class TestSampleModel:
    """Test the sample_model function."""

    def test_result(self):
        """Result matches the sample."""
        data = np.load(DATA_DIR / "model_sample.npz")
        inputs = {name: data[name] for name in (
            'size',
            'maxlevel',
            'rate',
        )}
        outputs = {name: data[name] for name in (
            'mean_exact',
            'mean_approx',
        )}
        expected = sociophysics.experiment.Result(inputs, outputs)
        actual = sociophysics.voting.sample_model(*expected.inputs.values())
        assert actual == expected
